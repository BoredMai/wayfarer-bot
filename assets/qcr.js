const Assets = {
  ERROR: {
    ACTIVE_PATRON: 'Patron $0 is already active.',
    NOT_FOUND: 'Cannot find patron with name $0.',
    RETIRED_PATRON: 'Patron $0 is already retired.'
  },
  MESSAGE: {
    PATRON_ADDED: 'Added patron $0 to the QCR.',
    PATRON_REMOVED: 'Removed patron $0\n<https://www.reddit.com$1>',
    QCR: `**$0**
    
**Status:** $1
**Reddit:** \`/u/$2\`
**Created on:** $3
**QCR:** <$4>`
  }
};

module.exports = Assets;
