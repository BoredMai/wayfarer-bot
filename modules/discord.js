const path = require('path');
const Discord = require('discord.js');
const QCR = require('./qcr.js');
const Quests = require('./quests.js');
const { Constants, Helpers } = require('./utils.js');
const Credentials = require('../credentials/discord.js');
const {
  COMMANDS,
  ERROR,
  MESSAGE,
  MODULES,
  PREFIX,
  PRICE,
} = require('../assets/discord.js');
const Icons = require('../assets/icons');
const Client = new Discord.Client();
const IconFiles = {};
let Contact = {};
let Logger = {};
let Channels = {};

class DiscordBot {
  async setup(logger, version) {
    Logger = logger;

    for (let key in Icons) {
      IconFiles[key] = new Discord.Attachment(
        path.resolve('assets/icons', Icons[key]),
      );
    }

    try {
      await Client.login(Credentials.BOT_SECRET);

      if (Credentials.USERNAME) {
        Client.user.setPresence({ game: { name: `v${version}` } });
      }

      for (let type in Credentials.CHANNELS) {
        this.loadChannel(type);
      }

      if (Credentials.CONTACT) {
        Contact = await Client.fetchUser(Credentials.CONTACT);
        await Contact.createDM();
      }

      Client.on('message', this.commandHandler.bind(this));
    } catch (err) {
      console.log(err);
    }
  }

  loadChannel(type) {
    const channel = Client.channels.get(Credentials.CHANNELS[type]);
    if (channel) {
      Channels[type] = channel;
      console.log(
        `[DISCORD] ${channel.name} | ${type} | ${channel.guild.name}`,
      );
    } else {
      Logger.error(
        `Channel not found | Type: ${type} | ID: ${Credentials.CHANNELS[type]}`,
      );
    }
  }

  hasAdminRole(member) {
    if (!member) {
      return false;
    }
    const intersection = member._roles.filter(role =>
      Credentials.ADMIN_ROLES.includes(role),
    );
    return intersection.length > 0;
  }

  async commandHandler({ author: { id, username }, channel, content, member }) {
    // Ignores its own messages, plus messages that don't start with PREFIX
    const [prefix, command, ...args] = content.match(/".*"|[^ ]+/g) || [];

    if (
      id === Client.user.id ||
      !prefix ||
      !PREFIX.includes(prefix.toLowerCase())
    )
      return;

    const isAdmin = this.hasAdminRole(member);

    if (!COMMANDS[command]) {
      const error = Helpers.formatMessage(ERROR.COMMAND_NOT_FOUND, command);
      return channel.send(this.getEmbedError(error));
    }

    const { adminOnly, numArgs } = COMMANDS[command];

    if (!isAdmin && adminOnly) {
      Logger.error(`Permission denied | ${username} | ${content}`);
      const error =
        channel.type === 'dm' ? ERROR.GUILD_ADMIN_ONLY : ERROR.FORBIDDEN;
      return channel.send(this.getEmbedError(error));
    }

    if (args.length < numArgs) {
      Logger.error(`Missing parameters | ${username} | ${content}`);

      const error = Helpers.formatMessage(
        ERROR.MISSING_PARAMS,
        PREFIX,
        command,
      );
      return channel.send(this.getEmbedError(error));
    }

    channel.startTyping();
    try {
      let fn;
      if (MODULES.QCR.includes(COMMANDS[command])) {
        fn = this.handleQCR.name;
      } else if (MODULES.QUESTS.includes(COMMANDS[command])) {
        fn = this.handleQuests.name;
      } else {
        fn = this.handleDiscord.name;
      }

      const embed = await this[fn](command, args, isAdmin);
      Logger.info(
        `Command executed | ${username} | ${content}\n${JSON.stringify(embed)}`,
      );
      embed && channel.send(embed);
    } catch ({ message }) {
      Logger.error(`Command failed | ${username} | ${content}\n${message}`);
      const embed = this.getEmbedError(ERROR.INTERNAL_ERROR);
      channel.send(embed);
      this.report(content, username, message);
    } finally {
      channel.stopTyping();
    }
  }

  getEmbedError(description) {
    const embed = this.getEmbed(Constants.EMBED_TYPE.ERROR, description);
    embed.author.name = Constants.ERROR_PREFIX;
    return embed;
  }

  getEmbedInfo(description, name = '', url = '') {
    const embed = this.getEmbed(Constants.EMBED_TYPE.INFO, description);
    embed.author.name = name;
    embed.author.url = url;
    return embed;
  }

  getEmbedSuccess(description) {
    const embed = this.getEmbed(Constants.EMBED_TYPE.SUCCESS, description);
    embed.author.name = Constants.SUCCESS_PREFIX;
    return embed;
  }

  getEmbedWarning(description, name = '') {
    const embed = this.getEmbed(Constants.EMBED_TYPE.WARNING, description);
    embed.author.name = name;
    return embed;
  }

  getEmbed(type, description = '') {
    const embed = new Discord.RichEmbed();
    embed.color = Constants.COLORS[type];
    embed.author = {
      icon_url: `attachment://${Icons[type]}`,
    };
    embed.description = description;
    embed.files = [IconFiles[type]];

    return embed;
  }

  handleDiscord(command, args, isAdmin) {
    return command === 'help'
      ? this.handleHelp(args, isAdmin)
      : this.handlePrice(args);
  }

  handleHelp([command], isAdmin) {
    const [prefix, ...aliases] = PREFIX;

    if (command) {
      const helpCommand = `${prefix} help ${command}`;

      if (!COMMANDS[command]) {
        const error = Helpers.formatMessage(
          ERROR.COMMAND_NOT_FOUND,
          helpCommand,
        );
        return this.getEmbed(error);
      }

      const { adminOnly, help } = COMMANDS[command];
      if (!isAdmin && adminOnly) {
        const error = Helpers.formatMessage(
          ERROR.COMMAND_NOT_FOUND,
          helpCommand,
        );
        return this.getEmbedError(error);
      }

      const embed = this.getEmbedInfo(
        Helpers.formatMessage(help, prefix),
        helpCommand,
      );
      embed.author.name = helpCommand;
      return embed;
    }

    const embed = this.getEmbedInfo('', MESSAGE.HELP_HEADER);

    embed.addField('Command Prefix', prefix, true);
    embed.addField('Aliases', aliases.join(', '), true);
    embed.addBlankField();

    for (let key in COMMANDS) {
      const { adminOnly, help } = COMMANDS[key];
      if (help && (!adminOnly || isAdmin)) {
        const [example, message] = help.split(' - ');
        const title = adminOnly ? Constants.ADMIN_PREFIX + example : example;
        embed.addField(Helpers.formatMessage(title, prefix), message);
      }
    }
    return embed;
  }

  handlePrice([rarity, ...itemName]) {
    const upper = rarity.toUpperCase();
    const data = PRICE[upper];

    if (!data) {
      const error = Helpers.formatMessage(ERROR.PRICE_INVALID_RARITY, rarity);
      return this.getEmbedError(error);
    }

    if (upper === 'VERY' && itemName[0].toUpperCase === 'RARE') {
      itemName.shift();
    }

    const { max, min, multiplier, rarityName } = data;

    const basePrice = Math.floor(Math.random() * (max - min)) + min;
    const price = basePrice * multiplier;

    Logger.info(
      `Price | ${JSON.stringify({
        rarityName,
        min,
        max,
        multiplier,
        basePrice,
        price,
      })}`,
    );
    let title = 'Price Info ';
    if (itemName.length) {
      title += `- ${itemName.join(' ')}`;
    }

    const embed = this.getEmbedInfo('', title);
    embed.addField('Rarity', rarityName, true);
    embed.addField('Price', price, true);

    return embed;
  }

  async handleQCR(command, args) {
    const name = args.join(' ');
    const { data, error, message } = await QCR.fromDiscord(command, name);

    if (error) {
      return this.getEmbedError(message);
    }

    let embed;

    if (data) {
      if (data.length === 1) {
        const { author, createdOn, fullName, status, url } = data.pop();
        embed = this.getEmbedInfo('', 'QCR Info')
          .setTitle(fullName)
          .setURL(url)
          .addField('Status', status, true)
          .addField(
            'Reddit User',
            `[/u/${author}](https://www.reddit.com/u/${author})`,
            true,
          )
          .addField('Created on', createdOn, true);
      } else {
        const warningTitle = Helpers.formatMessage(
          MESSAGE.MULTIPLE_RESULTS_FOUND,
          data.length,
        );
        embed = this.getEmbedWarning('', warningTitle);

        data.slice(0, 10).map(({ fullName, url }) => {
          embed.addField(fullName, url);
        });

        if (data.length > 10) {
          const warningFooter = Helpers.formatMessage(
            MESSAGE.MULTIPLE_RESULTS_FOOTER,
            data.length - 10,
          );
          embed.setFooter(warningFooter);
        }
      }
    } else {
      embed = this.getEmbedSuccess(message);
    }

    return embed;
  }

  async handleQuests(command, args) {
    const { error, message } = await Quests.fromDiscord(command, args);
    if (error) {
      return this.getEmbedError(message);
    }

    return this.getEmbedSuccess(message);
  }

  async broadcast(thread, type = 'THREAD') {
    const { author, selftext, title, url } = thread;
    try {
      const channel =
        type === 'WRAPUP' ? Channels.QUEST : Channels[type] || Channels.THREAD;
      const result = await channel.fetchMessages({ limit: 10 });
      const search = result.find(message => message.content.search(url) > -1);

      if (!search) {
        const info = Helpers.formatMessage(
          MESSAGE.BROADCAST,
          type.toLowerCase(),
        );

        const content =
          selftext.length > Constants.MAX_EMBED_LENGTH
            ? selftext.slice(0, Constants.MAX_EMBED_LENGTH - 3) + '...'
            : selftext;

        const embed = new Discord.RichEmbed()
          .setColor(Constants.COLORS.INFO)
          .setAuthor(info)
          .setTitle(title)
          .setURL(url)
          .setDescription(content)
          .addBlankField()
          .addField('Author', `/u/${author.name}`)
          .setTimestamp();

        if (type.match(/(IN|OU)TRO/)) {
          channel.send('@here');
        }
        channel.send(embed);
        Logger.info(info, author, title, url);

        if (type === 'OUTRO') {
          this.broadcast(thread);
        }
      }
    } catch (err) {
      Logger.error(Helpers.formatMessage(ERROR.BROADCAST, type, err, url));
      this.report(url, 'BROADCAST', err.message);
    }
  }

  report(source, author, message) {
    const embed = this.getEmbedError();
    embed
      .addField('Source', source, true)
      .addField('Author', author, true)
      .addField('Message', message);

    Contact.dmChannel.send(embed);
  }
}

module.exports = new DiscordBot();
