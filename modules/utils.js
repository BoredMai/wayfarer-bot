const Constants = {
  ADMIN_PREFIX: '🔒',
  COLORS: {
    ERROR: 0xcc0000,
    INFO: 0x44aaff,
    SUCCESS: 0x00aa00,
    WARNING: 0xffcc33
  },
  EMBED_TYPE: {
    ERROR: 'ERROR',
    INFO: 'INFO',
    SUCCESS: 'SUCCESS',
    WARNING: 'WARNING'
  },
  ERROR_PREFIX: 'Error',
  MAX_EMBED_LENGTH: 1024,
  MAX_TIME_DIFF: 30 * 1000, // 1/2 minute in milliseconds
  QCR_STATUS: {
    ACTIVE: 'Active',
    RETIRED: 'Retired'
  },
  QUEST_STATUS: {
    CLAIMED: 'Claimed',
    FINISHED: 'Finished',
    UNCLAIMED: 'Unclaimed'
  },
  SUCCESS_PREFIX: 'Success'
};

const Helpers = {
  addErrorPrefix: message => {
    return `${Constants.ERROR_PREFIX}\n\n${message}`;
  },
  cleanQCRName: name => {
    return name
      .trim()
      .replace(/([\*\#])/g, '')
      .replace(/^names? ?:?/i, '')
      .replace(/[‘’]/g, "'")
      .trim();
  },
  isThreadOld: created => {
    const timeDiff = Date.now() - created * 1000;
    return timeDiff > Constants.MAX_TIME_DIFF;
  },
  getNameFromQCR: body => {
    const [firstLine, secondLine] = body.split(/\n{1,2}/);
    return Helpers.cleanQCRName(firstLine) || Helpers.cleanQCRName(secondLine);
  },
  getUrlFromComment: ({ id, link_id }) => {
    return `https://www.reddit.com/comments/${link_id.slice(3)}/_/${id}`;
  },
  formatMessage: (message, ...args) => {
    args.forEach((arg, index) => {
      message = message.replace(`$${index}`, arg);
    });
    return message;
  },
  removeLink: string => string.replace(/[\[\]\\]|\(.*\)/g, '')
};

module.exports = { Constants, Helpers };
