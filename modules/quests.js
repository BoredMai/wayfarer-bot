const { MESSAGE, ERROR } = require('../assets/quests.js');
const { Constants, Helpers } = require('./utils.js');
let Wrap = {};
let Thread = {};

class Quests {
  setWrap(wrap) {
    Wrap = wrap;
    return this;
  }

  async setThread(id) {
    try {
      Thread = await Wrap.getSubmission(id);
      const title = await Thread.title;
      console.log(`[QUESTS] Thread ID: ${id} | Title: ${title}`);
      return Thread;
    } catch (err) {
      throw err;
    }
  }

  getIdFromThread(text) {
    const regex = /\[(?:ID)?(\d*)\\?\]/gi;
    const match = regex.exec(text);

    // match[0] is the full tag (ex.: '[ID001]', '[001]', '[id1]')
    // match[1] is the number matched (ex.: '001', '1')
    return match ? parseInt(match[1]).toString() : null;
  }

  /**
   * Quest string format:
   * |ID|Quest Titles|Difficulty/Reward(s)|Status|(...)
   */

  getQuestInfo(questString) {
    const [, id, title] = questString.split('|');
    return [id, Helpers.removeLink(title)];
  }

  getQuestStatus(questString) {
    const questArray = questString.split('|');
    return Helpers.removeLink(questArray[4]);
  }

  updateQuestStatus(questString, status) {
    const questArray = questString.split('|');
    questArray[4] = status;
    return questArray.join('|');
  }

  async fromReddit({ selftext, url }, tag) {
    const id = this.getIdFromThread(selftext);
    if (!id) {
      return { error: true, message: Helpers.formatMessage(ERROR.NO_ID) };
    }

    const action = tag === 'QUEST' ? 'claim' : 'wrap';
    return this.execute(action, id, url);
  }

  async fromDiscord(action, args) {
    const [id, rawUrl] = args;
    const url =
      rawUrl && rawUrl.length === 6
        ? `https://www.reddit.com/comments/${rawUrl}`
        : rawUrl;

    return this.execute(action, id, url);
  }

  async execute(action, id, url) {
    try {
      let content = await Thread.refresh().selftext;

      const regex = new RegExp(`\\|0*${id}\\|.*`);
      const [questString] = content.match(regex) || [null];

      if (!questString) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.NOT_FOUND, id)
        };
      }

      const status = this.getQuestStatus(questString);
      const { message, newStatus } = this[action](status, url) || {};
      if (newStatus) {
        const newQuestString = this.updateQuestStatus(questString, newStatus);
        content = content.replace(questString, newQuestString);
        await Thread.edit(content);
        return {
          message: Helpers.formatMessage(
            message,
            ...this.getQuestInfo(newQuestString)
          )
        };
      }

      return {
        error: true,
        message: Helpers.formatMessage(
          ERROR.UNAVAILABLE,
          action,
          status.toLowerCase()
        )
      };
    } catch (err) {
      throw err;
    }
  }

  claim(status, url) {
    if (status === Constants.QUEST_STATUS.UNCLAIMED) {
      return {
        message: MESSAGE.CLAIM,
        newStatus: `[[${Constants.QUEST_STATUS.CLAIMED}]](${url})`
      };
    }
  }

  drop(status) {
    if (status === Constants.QUEST_STATUS.CLAIMED) {
      return {
        message: MESSAGE.DROP,
        newStatus: `[${Constants.QUEST_STATUS.UNCLAIMED}]`
      };
    }
  }

  wrap(status, url) {
    if (status === Constants.QUEST_STATUS.CLAIMED) {
      return {
        message: MESSAGE.WRAP,
        newStatus: `[[${Constants.QUEST_STATUS.FINISHED}]](${url})`
      };
    }
  }
}

module.exports = new Quests();
